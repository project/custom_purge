<?php

namespace Drupal\custom_purge\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Flood\DatabaseBackend;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\custom_purge\Purger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The form to purge single URLs.
 */
class UrlPurgeForm extends FormBase {

  /**
   * The flood control mechanism.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The purger.
   *
   * @var \Drupal\custom_purge\Purger
   */
  protected $purger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $dbConnection;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood control mechanism.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date service.
   * @param \Drupal\custom_purge\Purger $purger
   *   The Url purger.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Database\Connection $db_connection
   *   The database connection.
   */
  public function __construct(FloodInterface $flood, DateFormatterInterface $date_formatter, Purger $purger, ModuleHandlerInterface $module_handler, Connection $db_connection) {
    $this->flood = $flood;
    $this->dateFormatter = $date_formatter;
    $this->purger = $purger;
    $this->moduleHandler = $module_handler;
    $this->dbConnection = $db_connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('custom_purge.flood'),
      $container->get('date.formatter'),
      $container->get('custom_purge.purger'),
      $container->get('module_handler'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_purge_url_purger';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get config settings.
    $config = $this->config('custom_purge.settings');
    $profile_key = 'profiles.' . $config->get('profile') . '.';
    $max_url_per_request = $config->get($profile_key . 'max_url_per_request');

    // Flood protection.
    $this->flood->garbageCollection();
    $limit = $config->get($profile_key . 'flood_limit');
    // Get interval - convert to seconds.
    $interval = $config->get($profile_key . 'flood_interval') * 60 * 60;
    // Number of flood entries.
    $flood_count = $this->getFloodCount();

    if ($flood_count !== FALSE) {
      $flood_info = $this->t('You already cleared %flood_count of %limit cache entries in @interval.', [
        '%limit' => $limit,
        '@interval' => $this->dateFormatter->formatInterval($interval),
        '%flood_count' => $flood_count,
      ]);
      $form['cache_clear_info'] = [
        '#markup' => $flood_info,
      ];
    }

    // In case of reached flood count we disable the form and don't allow
    // further submissions.
    $disabled = FALSE;
    if ($flood_count >= $limit) {
      $disabled = TRUE;
      // Show message to users.
      $this->messenger()->addWarning($this->t('You cannot clear more than %limit cache entries in @interval. Try again later.', [
        '%limit' => $limit,
        '@interval' => $this->dateFormatter->formatInterval($interval),
      ]));
    }

    // Textarea to enter urls to be purged after submitting the form.
    $form['purgable_urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('URLs that will be purged from caches'),
      '#description' => $this->t('Maximum number of purgable URLs: <b>@max_number</b><br/>Enter one IP per line.', ['@max_number' => $max_url_per_request]),
      '#required' => TRUE,
      '#rows' => 20,
    ];

    // Submit the form.
    $form['purge_submit'] = [
      '#type' => 'submit',
      '#value' => t('Purge urls from caches'),
      '#button_type' => 'primary',
      '#disabled' => $disabled,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $config = $this->config('custom_purge.settings');
    $profile_key = 'profiles.' . $config->get('profile') . '.';

    // Flood protection.
    $limit = $config->get($profile_key . 'flood_limit');
    // Get interval - convert to seconds.
    $interval = $config->get($profile_key . 'flood_interval') * 60 * 60;

    if (!$this->flood->isAllowed('custom_purge_url_purger', $limit, $interval, 'custom_purge_url_purger')) {
      $form_state->setError($form, $this->t('You cannot clear more than %limit cache entries in @interval. Try again later.', [
        '%limit' => $limit,
        '@interval' => $this->dateFormatter->formatInterval($interval),
      ]));
    }

    // Validation for max_url_per_request.
    $max_url_per_request = $config->get($profile_key . 'max_url_per_request');
    // Get urls to be pruged.
    $urls = preg_split('/\r\n|\r|\n/', $form_state->getValue('purgable_urls'));
    // Prevent empty lines in the form input from creating empty url entries.
    $urls = array_filter($urls);

    // Check for url count.
    if (count($urls) <= $max_url_per_request) {
      foreach ($urls as $url) {
        // Check for valid urls - if less/ equal then max_url_per_request.
        if (!empty($url) && !filter_var($url, FILTER_VALIDATE_URL)) {
          $form_state->setErrorByName('purgable_urls', $this->t('Please provide valid urls in form below.'));
        }
      }
    }
    else {
      $form_state->setErrorByName('purgable_urls', $this->t('Maximum number of purgable URLs @max_numer. Please enter less or equal.', ['@max_numer' => $max_url_per_request]));
    }

    // Set urls to be purged in form-state.
    $form_state->set('purgable_urls_array', $urls);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('custom_purge.settings');
    $profile_key = 'profiles.' . $config->get('profile') . '.';

    // Get urls from form_state.
    $urls = $form_state->get('purgable_urls_array');

    // No further processing if urls are empty - just show warning.
    if (empty($urls)) {
      $this->messenger()->addWarning(t('No url(s) were provided'));
      return;
    }

    $this->purger->setMessenger($this->messenger());
    $this->purger->purgeUrls($urls);

    // Register flood event for each url.
    $flood_interval = $config->get($profile_key . 'flood_interval') * 60 * 60;
    for ($i = 0; $i < count($urls); $i++) {
      $this->flood->register('custom_purge_url_purger', $flood_interval, 'custom_purge_url_purger');
    }
  }

  /**
   * Get flood counts for given identifier.
   *
   * @return int|bool
   *   The current flood count, nor FALSE if not known.
   */
  protected function getFloodCount() {
    // A flood count only works when using the database backend.
    if (!($this->flood instanceof DatabaseBackend) || !$this->dbConnection->schema()->tableExists('flood')) {
      return FALSE;
    }

    // Custom db query to get row count for checking flood protection.
    $query = $this->dbConnection->select(DatabaseBackend::TABLE_NAME, 'f');
    $query->addField('f', 'fid');
    $query->condition('event', 'custom_purge_url_purger');
    $query->condition('identifier', 'custom_purge_url_purger');
    return $query->countQuery()->execute()->fetchField();
  }

}
