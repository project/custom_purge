<?php

namespace Drupal\custom_purge\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for custom purge plugins.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class PurgePlugin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
