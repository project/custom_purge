<?php

namespace Drupal\custom_purge;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\custom_purge\Plugin\DomainSensitiveCacheInterface;
use Drupal\custom_purge\Plugin\PurgePluginManager;
use Psr\Log\LogLevel;

/**
 * The service for custom purging.
 */
class Purger {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The (optional) messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The purge plugin manager.
   *
   * @var \Drupal\custom_purge\Plugin\PurgePluginManager
   */
  protected $purgePluginManager;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The Purger constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\custom_purge\Plugin\PurgePluginManager $purge_plugin_manager
   *   The purge plugin manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, LoggerChannelFactoryInterface $logger_factory, PurgePluginManager $purge_plugin_manager, QueueFactory $queue_factory, TimeInterface $time) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->logger = $logger_factory->get('custom_purge');
    $this->purgePluginManager = $purge_plugin_manager;
    $this->queueFactory = $queue_factory;
    $this->time = $time;
  }

  /**
   * Set the messenger.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface|null $messenger
   *   The messenger. Set to NULL to unset the messenger.
   */
  public function setMessenger(MessengerInterface $messenger = NULL) {
    $this->messenger = $messenger;
  }

  /**
   * Purge a given list of urls.
   *
   * @param string[] $urls
   *   The list of absolute urls as strings.
   * @param array $caches
   *   (Optional) Use this argument to restrict to a certain subset of cache
   *   instances to purge. Example: ['type' => ['varnish']] Purges the urls only
   *   at known Varnish instances. ['name' => ['my_varnish_instance']] Purges
   *   the urls only at a known instance having the specified name.
   * @param bool $enqueue
   *   (Optional) Set this parameter to TRUE if you want to use the configured
   *   delays in seconds like when purging everything. Items will be thrown
   *   into a queue (namely "custom_purge_urls") and then be executed on next
   *   cron or explicit queue:run command. Read more about the queue logic at
   *   ::enqueuePurgeEverything().
   * @param int $add_delay
   *   (Optional) Additional delay in seconds, that will be added to every cache
   *   purge plugin (including those that are set to 0). This parameter will
   *   only take effect, when the $enqueue parameter is set to TRUE.
   *
   * @return array
   *   An array of processed, failed and enqueued urls, keyed by 'processed',
   *   'errors', 'skipped' and 'queued'. Each of those three groups is further
   *   grouped by purge plugins for the corresponding caching instance.
   */
  public function purgeUrls(array $urls, array $caches = [], $enqueue = FALSE, $add_delay = 0) {
    $config = $this->getConfig();
    $profile_key = 'profiles.' . $config->get('profile') . '.';
    $domains_settings = $config->get($profile_key. 'domains') ?: [];
    $info = [
      'processed' => [],
      'errors' => [],
      'skipped' => [],
      'queued' => [],
    ];
    if (empty($domains_settings) || !is_array($domains_settings)) {
      $this->log(LogLevel::ERROR, $this->t('The custom_purge.settings config does not have a domain entry. Please make sure it consists at least of one default domain settings entry.'));
      return $info;
    }

    $default_domain = NULL;
    $domains = [];
    $protocols = [];

    foreach ($domains_settings as $domain_settings) {
      if (!empty($domain_settings['is_default']) && empty($default_domain)) {
        $default_domain = $domain_settings['domain'];
      }
      $domains[] = $domain_settings['domain'];
      $protocols[$domain_settings['domain']] = $domain_settings['default_protocol'];
    }
    if (empty($default_domain)) {
      $this->log(LogLevel::ERROR, $this->t('The custom_purge.settings config does not have a default domain entry. Please set one domain settings entry as the default one.'));
      return $info;
    }

    // Sort by length descending, so that matchings of longer domains will
    // take precedence (e.g. a subdomain www.example.com should match for
    // a given url with exactly that domain, whereas example.com may match
    // for others like sub.example.com).
    usort($domains, function ($a, $b) {
      return strlen($b) - strlen($a);
    });

    $urls_by_domain = [];
    foreach ($urls as $url) {
      $url = trim($url);
      foreach ($domains as $domain) {
        if (strpos($url, $domain) !== FALSE && str_starts_with($url, $protocols[$domain])) {
          // Both default protocol and domain match.
          $urls_by_domain[$domain][] = $url;
          continue 2;
        }
      }

      foreach ($domains as $domain) {
        if (strpos($url, $domain) !== FALSE) {
          // Domain matches, but not the protocol.
          $urls_by_domain[$domain][] = $url;
          continue 2;
        }
      }

      // No domain settings matched, extract the host and use the default domain
      // settings for it.
      $domain = parse_url($url, PHP_URL_HOST) ?: $default_domain;
      $urls_by_domain[$domain][] = $url;
    }

    if ($enqueue) {
      $queue = $this->queueFactory->get('custom_purge_urls', TRUE);
      $queue->createQueue();
    }

    foreach ($urls_by_domain as $domain => $domain_urls) {
      /** @var \Drupal\custom_purge\Plugin\PurgePluginInterface $purge_plugin */
      foreach ($this->purgePluginManager->getPurgePluginsForDomain($domain, $caches) as $purge_plugin) {
        $plugin_settings = $purge_plugin->getPurgePluginSettings();
        $cache_name = $plugin_settings->getCacheName();
        $item_key = $cache_name;
        if ($purge_plugin instanceof DomainSensitiveCacheInterface) {
          $item_key .= '__' . $purge_plugin->getDomainSettings()->getDefaultProtocol() . '__' . $purge_plugin->getDomainSettings()->getDomain();
        }
        if (!$plugin_settings->isAllowedToPurgeSingleUrls()) {
          $info['skipped'][$item_key] = $domain_urls;
          continue;
        }
        $delay = $enqueue ? $plugin_settings->getDelayCompletePurge() + $add_delay : 0;
        if ($delay > 0) {
          $queue->createItem([
            'urls' => $domain_urls,
            'target_time' => $this->time->getCurrentTime() + $delay,
            'cache_name' => $cache_name,
            'item_key' => $item_key,
          ]);
          $info['queued'][$item_key] = $domain_urls;
          continue;
        }
        $purge_result = $purge_plugin->purgeUrls($domain_urls);
        if (!empty($purge_result['processed'])) {
          $this->log(LogLevel::INFO, $this->t('Purged @num urls from @type cache "@name" of domain "@domain".', [
            '@num' => count($purge_result['processed']),
            '@type' => $purge_plugin->getPluginId(),
            '@name' => $cache_name,
            '@domain' => $domain,
          ]));
          $info['processed'][$item_key] = $purge_result['processed'];
        }
        if (!empty($purge_result['errors'])) {
          $this->log(LogLevel::ALERT, $this->t('Failed to purge @num urls from @type cache "@name" of domain "@domain".', [
            '@num' => count($purge_result['errors']),
            '@type' => $purge_plugin->getPluginId(),
            '@name' => $cache_name,
            '@domain' => $domain,
          ]));
          $info['errors'][$item_key] = $purge_result['errors'];
        }
      }
    }

    // Allow other modules to act upon the manual purge of Urls.
    $this->moduleHandler->invokeAll('manual_custom_purge', [$urls]);
    return $info;
  }

  /**
   * Enqueues purging of everything.
   *
   * This method reads from the "delay_complete_purge" for each configured
   * cache purge plugin. If that key is set to 0, then the complete purge is
   * being executed immediately. Any other plugin will be thrown into a queue
   * (namely "custom_purge_everything"), and is supposed to be executed on the
   * next cron or an explicit queue:run command when it reached the specified
   * delay. It should be noted that the start after the specified would not be
   * immediate, because it depends solely on when the next cron or queue worker
   * run is being executed.
   *
   * @param array $domains
   *   (Optional) Only run for specific domains (that are known from
   *   configuration). When not given, caches of all domains known from the
   *   configuration will be completely purged.
   * @param array $caches
   *   (Optional) Use this argument to restrict to a certain subset of cache
   *   instances to purge. Example: ['type' => ['varnish']] Purges the urls only
   *   at known Varnish instances. ['name' => ['my_varnish_instance']] Purges
   *   the urls only at a known instance having the specified name.
   * @param int $add_delay
   *   (Optional) Additional delay in seconds, that will be added to every cache
   *   purge plugin (including those that are set to 0). This might be handy if
   *   you want to just setup a complete purge operation that would be
   *   automatically executed at the specified delayed time.
   * @param bool $ignore_existing_items
   *   (Optional) Normally, this method checks for existing items in the queue
   *   and only adds cache purge plugins that are not yet added. When this
   *   option is set to TRUE, then all items will be enqueued, regardless
   *   whether they are already in the queue.
   * @param bool $reset_queue
   *   (Optional) When this option is set to TRUE, all remaining items (if any)
   *   will be dropped from the queue, before throwing cache purge plugins into
   *   it.
   * @param bool $ignore_delay_run_immediately
   *   When this parameter is set to TRUE, no items will be enqueued. Instead,
   *   all configured cache purge plugins (or the ones set by the $caches
   *   parameter) will be executed immediately (in the sequential order of the
   *   configuration).
   *
   * @return array
   *   An array of processed, failed, skipped and enqueued purge calls, keyed by
   *   'processed', 'errors', 'skipped' and 'queued'. Each group is an array of
   *   cache names of the instances.
   */
  public function enqueuePurgeEverything(array $domains = [], array $caches = [], $add_delay = 0, $ignore_existing_items = FALSE, $reset_queue = FALSE, $ignore_delay_run_immediately = FALSE) {
    $config = $this->getConfig();
    $profile_key = 'profiles.' . $config->get('profile') . '.';
    $info = [
      'processed' => [],
      'errors' => [],
      'skipped' => [],
      'queued' => [],
    ];
    $configured_domains = [];
    foreach ($config->get($profile_key . 'domains') as $domain_settings) {
      $configured_domains[] = $domain_settings['domain'];
    }
    if (empty($domains)) {
      $domains = $configured_domains;
    }
    else {
      foreach ($domains as $i => $domain) {
        if (!in_array($domain, $configured_domains)) {
          $this->log(LogLevel::ERROR, $this->t("The domain '@domain' is not present in the custom_purge.settings config. Please make sure that it is present and properly configured.", ['@domain' => $domain]));
          unset($domains[$i]);
        }
      }
    }
    if (empty($domains)) {
      $this->log(LogLevel::ERROR, $this->t("No domains were found that could be purged."));
      return $info;
    }

    $purge_plugins = [];
    foreach ($domains as $domain) {
      foreach ($this->purgePluginManager->getPurgePluginsForDomain($domain, $caches) as $purge_plugin) {
        if (!in_array($purge_plugin, $purge_plugins, TRUE)) {
          $purge_plugins[] = $purge_plugin;
        }
      }
    }

    $queue = $this->queueFactory->get('custom_purge_everything', TRUE);
    if ($reset_queue) {
      try {
        $queue->deleteQueue();
      }
      catch (\Exception $e) {
      }
    }
    $queue->createQueue();
    $existing_items = [];
    if (!$ignore_existing_items) {
      $queue_items = [];
      while ($queue_item = $queue->claimItem(60)) {
        $queue_items[] = $queue_item;
        $existing_items[$queue_item->data['item_key']] = TRUE;
      }
      // Immediately release those items for the queue workers.
      foreach ($queue_items as $queue_item) {
        $queue->releaseItem($queue_item);
      }
      // No more need for queue items from here.
      $queue_items = [];
    }

    /** @var \Drupal\custom_purge\Plugin\PurgePluginInterface $purge_plugin */
    foreach ($purge_plugins as $purge_plugin) {
      $plugin_settings = $purge_plugin->getPurgePluginSettings();
      $cache_name = $plugin_settings->getCacheName();
      $delay = $plugin_settings->getDelayCompletePurge() + $add_delay;
      $item_key = $cache_name;
      $scoped_domains = $domains;
      if ($purge_plugin instanceof DomainSensitiveCacheInterface) {
        $item_key .= '__' . $purge_plugin->getDomainSettings()->getDefaultProtocol() . '__' . $purge_plugin->getDomainSettings()->getDomain();
        $scoped_domains = [$purge_plugin->getDomainSettings()->getDomain()];
      }
      if (isset($existing_items[$item_key])) {
        continue;
      }
      if (!$plugin_settings->isAllowedToPurgeEverything()) {
        $info['skipped'][$item_key] = $cache_name;
        continue;
      }
      if ($ignore_delay_run_immediately || $delay == 0) {
        if ($purge_plugin->purgeEverything()) {
          $this->log(LogLevel::INFO, $this->t('Purged everything from @type cache "@name" (item key @item_key).', [
            '@type' => $purge_plugin->getPluginId(),
            '@name' => $cache_name,
            '@item_key' => $item_key,
          ]));
          $info['processed'][$item_key] = $cache_name;
        }
        else {
          $this->log(LogLevel::ALERT, $this->t('Failed to purge everything from @type cache "@name" (item key @item_key).', [
            '@type' => $purge_plugin->getPluginId(),
            '@name' => $cache_name,
            '@item_key' => $item_key,
          ]));
          $info['errors'][$item_key] = $cache_name;
        }
        continue;
      }
      $queue->createItem([
        'item_key' => $item_key,
        'cache_name' => $cache_name,
        'domains' => $scoped_domains,
        'target_time' => $this->time->getCurrentTime() + $delay,
      ]);
      $info['queued'][$item_key] = $cache_name;
    }

    return $info;
  }

  /**
   * Get a configuration object.
   *
   * @param string $name
   *   (Optional) The name of the config. Skip to get the custom_purge.settings.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The configuration object.
   */
  protected function getConfig($name = 'custom_purge.settings') {
    return $this->configFactory->get($name);
  }

  /**
   * Logs (and prints) a message.
   *
   * @param string $log_level
   *   A log level, as defined via constant from \Psr\Log\LogLevel.
   * @param string|\Drupal\Component\Render\MarkupInterface $message
   *   The translated message to log.
   * @param bool $include_messenger
   *   (Optional) Set to FALSE if you definetly not want to print the message.
   */
  protected function log($log_level, $message, $include_messenger = TRUE) {
    $this->logger->log($log_level, $message);
    if ($this->messenger && $include_messenger) {
      switch ($log_level) {
        case LogLevel::INFO:
          $this->messenger->addStatus($message);
          break;

        case LogLevel::NOTICE:
        case LogLevel::WARNING:
          $this->messenger->addWarning($message);
          break;

        case LogLevel::ERROR:
        case LogLevel::ALERT:
        case LogLevel::CRITICAL:
        case LogLevel::EMERGENCY:
          $this->messenger->addError($message);
          break;
      }
    }
  }

}
