<?php

namespace Drupal\custom_purge\Plugin;

/**
 * Helper class representing one entry of configured domains.
 */
final class DomainSettings {

  /**
   * The domain settings as array.
   *
   * @var array
   */
  private $settings;

  /**
   * The DomainSettings constructor.
   *
   * @param array $domain_settings
   *   The domain settings as array.
   */
  public function __construct(array $domain_settings) {
    $this->settings = $domain_settings;
  }

  /**
   * Get the domain value.
   *
   * @return string
   *   The domain.
   */
  public function getDomain() {
    return $this->settings['domain'];
  }

  /**
   * Get the default protocol.
   *
   * @return string
   *   The default protocol.
   */
  public function getDefaultProtocol() {
    return $this->settings['default_protocol'];
  }

  /**
   * Get the flag whether this domain settings entry is the default one.
   *
   * @return bool
   *   Returns TRUE when the domain settings entry is the default one.
   */
  public function getIsDefault() {
    return !empty($this->settings['is_default']);
  }

  /**
   * Get a specific entry from the settings.
   *
   * @param string|int $key
   *   The key of the settings entry.
   * @param mixed $default
   *   (Optional) The default value to use, if the entry does not exist.
   *
   * @return mixed
   *   The entry from the settings, or the default value if it does not exist.
   */
  public function get($key, $default = NULL) {
    return isset($this->settings[$key]) ? $this->settings[$key] : $default;
  }

}
