<?php

namespace Drupal\custom_purge\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for custom purge plugins.
 */
interface PurgePluginInterface extends PluginInspectionInterface {

  /**
   * Purge a given list of URLs.
   *
   * @param string[] $urls
   *   A list of absolute URLs as strings.
   *
   * @return array
   *   An array of processed and failed urls, keyed by 'processed' and 'errors'.
   */
  public function purgeUrls(array $urls);

  /**
   * Purge the whole caching instance.
   *
   * @return bool
   *   Whether the complete purge was successful or not.
   */
  public function purgeEverything();

  /**
   * Get the purge plugin settings.
   *
   * @return \Drupal\custom_purge\Plugin\PurgePluginSettings
   *   The purge plugin settings.
   */
  public function getPurgePluginSettings();

}
