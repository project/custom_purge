<?php

namespace Drupal\custom_purge\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for custom purge plugins.
 */
abstract class PurgePluginBase extends PluginBase implements PurgePluginInterface, ContainerFactoryPluginInterface {

  /**
   * The settings of this purge cache plugin.
   *
   * @var \Drupal\custom_purge\Plugin\PurgePluginSettings
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * Constructs a PurgeCachePluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->settings = new PurgePluginSettings($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getPurgePluginSettings() {
    return $this->settings;
  }

  /**
   * Get an http client instance.
   *
   * The object is being instantiated with Drupal's built-in defaults. The
   * configuration values may be overwritted by the given config.
   *
   * @param array $config
   *   (Optional) The client configuration.
   *
   * @return \GuzzleHttp\Client
   *   The http client instance.
   *
   * @see \Drupal\Core\Http\ClientFactory::fromOptions
   */
  protected static function httpClient(array $config = []) {
    return \Drupal::service('http_client_factory')->fromOptions($config);
  }

  /**
   * Get the logger channel.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger channel.
   */
  protected static function logger() {
    return \Drupal::logger('custom_purge');
  }

}
