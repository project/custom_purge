<?php

namespace Drupal\custom_purge\Plugin;

/**
 * Helper class representing one entry of configured cache instances.
 */
final class PurgePluginSettings {

  /**
   * The purge plugin settings as array.
   *
   * @var array
   */
  private $settings;

  /**
   * The PurgePluginSettings constructor.
   *
   * @param array $plugin_settings
   *   The purge plugin settings as array.
   */
  public function __construct(array $plugin_settings) {
    $this->settings = $plugin_settings;
  }

  /**
   * Get the cache type, which equals the plugin ID.
   *
   * @return string
   *   The cache type.
   */
  public function getCacheType() {
    return $this->settings['cache_type'];
  }

  /**
   * Get the name of the cache instance.
   *
   * @return string
   *   The name of the cache instance.
   */
  public function getCacheName() {
    return $this->settings['cache_name'];
  }

  /**
   * Get whether this plugin is allowed to purge single urls.
   *
   * @return bool
   *   Returns TRUE if it is allowed, FALSE otherwise.
   */
  public function isAllowedToPurgeSingleUrls() {
    return !empty($this->settings['allow_url_purge']);
  }

  /**
   * Get whether this plugin is allowed to purge everything.
   *
   * @return bool
   *   Returns TRUE if it is allowed, FALSE otherwise.
   */
  public function isAllowedToPurgeEverything() {
    return !empty($this->settings['allow_purge_everything']);
  }

  /**
   * Get the delay in seconds when executing a purge of everything.
   *
   * This value is relevant for the Drush command
   * "custom-purge:enqueue-purge-everything". When the delay is greater than 0,
   * on a complete purge call the cache plugin will be thrown into a queue
   * (namely "custom_purge_everything"), and will be processed on the next cron
   * and/or explicit queue worker run.
   *
   * @return int
   *   The delay in seconds.
   */
  public function getDelayCompletePurge() {
    return isset($this->settings['delay_complete_purge']) ? (int) $this->settings['delay_complete_purge'] : 0;
  }

  /**
   * Get a specific entry from the settings.
   *
   * @param string|int $key
   *   The key of the settings entry.
   * @param mixed $default
   *   (Optional) The default value to use, if the entry does not exist.
   *
   * @return mixed
   *   The entry from the settings, or the default value if it does not exist.
   */
  public function get($key, $default = NULL) {
    return isset($this->settings[$key]) ? $this->settings[$key] : $default;
  }

}
