<?php

namespace Drupal\custom_purge\Plugin;

/**
 * A trait for purge plugins implementing DomainSensitiveCacheInterface.
 */
trait DomainSensitiveCacheTrait {

  /**
   * The domain settings.
   *
   * @var \Drupal\custom_purge\Plugin\DomainSettings
   */
  protected $domainSettings;

  /**
   * Set the domain settings.
   *
   * @param \Drupal\custom_purge\Plugin\DomainSettings $settings
   *   The domain settings.
   */
  public function setDomainSettings(DomainSettings $settings) {
    $this->domainSettings = $settings;
  }

  /**
   * Get the domain settings.
   *
   * @return \Drupal\custom_purge\Plugin\DomainSettings
   *   The domain settings.
   */
  public function getDomainSettings() {
    return $this->domainSettings;
  }

}
