<?php

namespace Drupal\custom_purge\Plugin\QueueWorker;

/**
 * Performes enqueued purge of everything on one caching instance.
 *
 * @QueueWorker(
 *   id = "custom_purge_everything",
 *   title = @Translation("Custom Purge Everything"),
 *   cron = {"time" = 10}
 * )
 */
class CustomPurgeEverything extends CustomPurgeWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!isset($data['target_time']) || !$this->targetTimeReached($data)) {
      return;
    }
    $this->purger->enqueuePurgeEverything($data['domains'], ['name' => [$data['cache_name']]], 0, FALSE, FALSE, TRUE);
  }

}
