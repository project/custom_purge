<?php

namespace Drupal\custom_purge\Plugin\QueueWorker;

/**
 * Performes enqueued purge of single urls on one caching instance.
 *
 * @QueueWorker(
 *   id = "custom_purge_urls",
 *   title = @Translation("Custom Purge Urls"),
 *   cron = {"time" = 10}
 * )
 */
class CustomPurgeUrls extends CustomPurgeWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!isset($data['target_time']) || !$this->targetTimeReached($data)) {
      return;
    }
    $this->purger->purgeUrls($data['urls'], ['name' => [$data['cache_name']]]);
  }

}
