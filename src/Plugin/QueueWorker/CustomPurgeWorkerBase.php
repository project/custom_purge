<?php

namespace Drupal\custom_purge\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for custom purge queue worker plugins.
 */
abstract class CustomPurgeWorkerBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The purger service.
   *
   * @var \Drupal\custom_purge\Purger
   */
  protected $purger;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->purger = $container->get('custom_purge.purger');
    $instance->time = $container->get('datetime.time');
    $instance->queueFactory = $container->get('queue');
    return $instance;
  }

  /**
   * Checks if the target time is reached.
   *
   * A new queue item is being created if it has not yet reached.
   *
   * @param array $data
   *   The data of the queue item.
   *
   * @return bool
   *   Returns TRUE if target time has reached, FALSE otherwise.
   */
  protected function targetTimeReached(array $data) {
    if ($data['target_time'] - $this->time->getCurrentTime() > 0) {
      // The target time is still in the future, thus re-throw this item into
      // the queue and try later. The item is being re-created instead of
      // throwing a RequeueException, so that subsequent queue items may
      // be processed that might have a lower target time than this item.
      sleep(1);
      $queue = $this->queueFactory->get('custom_purge_everything', TRUE);
      $queue->createItem($data);
      return FALSE;
    }
    return TRUE;
  }

}
