<?php

namespace Drupal\custom_purge\Plugin\custom_purge\Purge;

use Drupal\custom_purge\Plugin\DomainSensitiveCacheInterface;
use Drupal\custom_purge\Plugin\DomainSensitiveCacheTrait;
use Drupal\custom_purge\Plugin\PurgePluginBase;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;

/**
 * The custom purge plugin for a Varnish cache.
 *
 * @PurgePlugin(
 *   id="varnish",
 *   label="Varnish"
 * )
 */
class VarnishPurgePlugin extends PurgePluginBase implements DomainSensitiveCacheInterface {

  use DomainSensitiveCacheTrait;

  /**
   * The http client for purging single urls.
   *
   * @var \GuzzleHttp\Client
   */
  protected $singlePurgeClient;

  /**
   * The Http method to use for purging single urls.
   *
   * @var string
   */
  protected $singlePurgeHttpMethod;

  /**
   * The http client for purging everything.
   *
   * @var \GuzzleHttp\Client
   */
  protected $purgeEverythingClient;

  /**
   * The request object used for purging everything.
   *
   * @var \GuzzleHttp\Psr7\Request
   */
  protected $purgeEverythingRequest;

  /**
   * {@inheritdoc}
   */
  public function purgeUrls(array $urls) {
    $info = ['processed' => [], 'errors' => []];
    $this->initializeSinglePurgeClient();
    foreach ($urls as $url) {
      if ($this->doSendSinglePurgeRequest($url)) {
        $info['processed'][] = $url;
      }
      else {
        $info['errors'][] = $url;
      }
    }
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function purgeEverything() {
    $this->initializePurgeEverythingClient();
    $response = NULL;
    try {
      $response = $this->purgeEverythingClient->send($this->purgeEverythingRequest);
    }
    catch (GuzzleException $e) {
      static::logger()->error(sprintf("Varnish purge call failed when trying to purge the whole cache on %s. The error was: %s", (string) $this->purgeEverythingRequest->getUri(), $e->getMessage()));
    }
    return $response && $response->getStatusCode() >= 200 && $response->getStatusCode() < 300;
  }

  /**
   * Executes a purge request for a single URL.
   *
   * @param string $url
   *   Single absolute url that will be purged from varnish.
   *
   * @return bool
   *   Whether the purge was successful or not.
   */
  protected function doSendSinglePurgeRequest($url) {
    $response = NULL;
    try {
      $response = $this->singlePurgeClient->request($this->singlePurgeHttpMethod, $url);
    }
    catch (GuzzleException $e) {
      static::logger()->error(sprintf("Varnish purge call failed when trying to purge url %s. The error was: %s", $url, $e->getMessage()));
    }
    return $response && $response->getStatusCode() >= 200 && $response->getStatusCode() < 300;
  }

  /**
   * Initializes the http client for purging single urls.
   */
  protected function initializeSinglePurgeClient() {
    if (isset($this->singlePurgeClient)) {
      return;
    }

    $settings_single = $this->settings->get('single');
    $client_config = $this->getCommonHttpClientConfig();

    if (!empty($settings_single['http_headers'])) {
      foreach ($settings_single['http_headers'] as $additional_header) {
        list($key, $value) = explode(':', $additional_header);
        $client_config['headers'][trim($key)] = trim($value);
      }
    }

    $this->singlePurgeClient = static::httpClient($client_config);
    $this->singlePurgeHttpMethod = !empty($settings_single['http_method']) ? $settings_single['http_method'] : 'PURGE';
  }

  /**
   * Initializes the http client for purging everything.
   */
  protected function initializePurgeEverythingClient() {
    if (isset($this->purgeEverythingClient)) {
      return;
    }

    $domain_settings = $this->getDomainSettings();
    $settings_everything = $this->settings->get('everything');
    $client_config = $this->getCommonHttpClientConfig();

    if (!empty($settings_everything['http_headers'])) {
      foreach ($settings_everything['http_headers'] as $additional_header) {
        list($key, $value) = explode(':', $additional_header);
        $client_config['headers'][trim($key)] = trim($value);
      }
    }

    $http_method = !empty($settings_everything['http_method']) ? $settings_everything['http_method'] : 'BAN';
    $uri = !empty($settings_everything['url']) ? $settings_everything['url'] : '/';
    if ((str_starts_with($uri, '/') && !str_starts_with($uri, '//')) || empty(parse_url($uri, PHP_URL_HOST))) {
      // Given url is relative, prepend the domain.
      $uri = $domain_settings->getDomain() . $uri;
    }
    if (!str_starts_with($uri, 'http') && ($protocol = $domain_settings->getDefaultProtocol())) {
      $protocol .= str_starts_with($uri, '//') ? ':' : '://';
      $uri = $protocol . $uri;
    }

    $this->purgeEverythingClient = static::httpClient($client_config);
    $this->purgeEverythingRequest = new Request($http_method, $uri);
  }

  /**
   * Get the common client config that is used on all http client instances.
   *
   * @return array
   *   The common http client config.
   */
  protected function getCommonHttpClientConfig() {
    $settings = $this->settings;
    $domain_settings = $this->getDomainSettings();

    $resolve = $domain_settings->getDomain() . ':' . $settings->get('port') . ':' . $settings->get('ip');

    $curl_options = [
      CURLOPT_RESOLVE => [$resolve],
    ];
    $verifyhost = (bool) $settings->get('verifyhost', TRUE);
    if (!$verifyhost) {
      $curl_options[CURLOPT_SSL_VERIFYHOST] = 0;
    }
    $verifypeer = (bool) $settings->get('verifypeer', TRUE);
    if (!$verifypeer) {
      $curl_options[CURLOPT_SSL_VERIFYPEER] = FALSE;
    }

    $client_config = [
      'connect_timeout' => 2.0,
      'timeout' => 3.0,
      'curl' => $curl_options,
      'headers' => [
        'Accept-Encoding' => 'gzip',
      ],
    ];
    if (!$verifyhost && !$verifypeer) {
      $client_config['verify'] = FALSE;
    }

    return $client_config;
  }

}
