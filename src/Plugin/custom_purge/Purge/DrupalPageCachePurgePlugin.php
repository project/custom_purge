<?php

namespace Drupal\custom_purge\Plugin\custom_purge\Purge;

use Drupal\custom_purge\Plugin\PurgePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The custom purge plugin for the Drupal page cache.
 *
 * @PurgePlugin(
 *   id="drupal_page_cache",
 *   label="Drupal page cache"
 * )
 */
class DrupalPageCachePurgePlugin extends PurgePluginBase {

  /**
   * The backend of the page cache, if it exists.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface|null
   */
  protected $cacheBackend;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    if ($container->has('cache.page')) {
      $instance->cacheBackend = $container->get('cache.page');
    }
    else {
      $instance->cacheBackend = NULL;
      static::logger()->warning(sprintf("The custom purge plugin '%s' was instantiated, but the page cache is not enabled. Consider removing this instance from the custom_purge.settings configuration.", $plugin_id));
    }
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function purgeUrls(array $urls) {
    if ($this->cacheBackend) {
      $cid_extensions = $this->settings->get('cid_extensions', [':']);

      $cids = [];
      foreach ($urls as $url) {
        foreach ($cid_extensions as $cid_extension) {
          $cids[] = $url . $cid_extension;
        }
      }

      $this->cacheBackend->deleteMultiple($cids);
    }

    return ['processed' => $urls, 'errors' => []];
  }

  /**
   * {@inheritdoc}
   */
  public function purgeEverything() {
    if ($this->cacheBackend) {
      $this->cacheBackend->deleteAll();
    }
    return TRUE;
  }

}
