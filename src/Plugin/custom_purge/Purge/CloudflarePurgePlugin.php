<?php

namespace Drupal\custom_purge\Plugin\custom_purge\Purge;

use Drupal\custom_purge\Plugin\PurgePluginBase;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The custom purge plugin for the Cloudflare cache.
 *
 * @PurgePlugin(
 *   id="cloudflare",
 *   label="Cloudflare"
 * )
 */
class CloudflarePurgePlugin extends PurgePluginBase {

  /**
   * The cloudflare API url, containing a placeholder for the zone ID.
   *
   * @var string
   */
  protected static $apiUrlPlaceholder = 'https://api.cloudflare.com/client/v4/zones/[:zone_id]/purge_cache';

  /**
   * The cloudflare API url, fulfilled with the zone ID.
   *
   * @var string
   */
  protected $apiUrl;

  /**
   * The zone ID.
   *
   * @var string
   */
  protected $zoneId;

  /**
   * The auth email.
   *
   * @var string
   */
  protected $authEmail;

  /**
   * The auth API key.
   *
   * @var string
   */
  protected $authKey;

  /**
   * A flag indicating whether the sttings are complete.
   *
   * @var bool
   */
  protected $settingsOk;

  /**
   * The http client for purging the cloudflare cache.
   *
   * @var \GuzzleHttp\Client
   */
  protected $purgeClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $module_handler */
    $module_handler = $container->get('module_handler');
    if ($module_handler->moduleExists('cloudflare') && $instance->settings->get('use_cf_settings')) {
      /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
      $config_factory = $container->get('config.factory');
      $cloudflare_config = $config_factory->get('cloudflare.settings');
      $instance->zoneId = $cloudflare_config->get('zone_id');
      $instance->authEmail = $cloudflare_config->get('email');
      $instance->authKey = $cloudflare_config->get('apikey');

      // If cloudflare module returns an array for zone_id, we need
      // to convert it to a string.
      if (is_array($instance->zoneId)) {
        $instance->zoneId = reset($instance->zoneId) ?: '';
      }
    }
    else {
      $instance->zoneId = $instance->settings->get('zone_id');
      $instance->authEmail = $instance->settings->get('email');
      $instance->authKey = $instance->settings->get('apikey');
    }
    $instance->apiUrl = str_replace('[:zone_id]', (string) $instance->zoneId, static::$apiUrlPlaceholder);
    $instance->settingsOk = !empty($instance->zoneId) && !empty($instance->authEmail) && !empty($instance->authKey);
    if (!$instance->settingsOk) {
      static::logger()->error(sprintf("The cache purge plugin '%s' was instantiated with incomplete settings. Please make sure they are correct within the custom_purge.settings configuration.", $plugin_id));
    }

    return $instance;
  }

  /**
   * Initializes the purge client.
   */
  protected function initializePurgeClient() {
    if (isset($this->purgeClient)) {
      return;
    }
    $client_config = [
      'connect_timeout' => 2.0,
      'timeout' => 3.0,
      'headers' => [
        'X-Auth-Email' => $this->authEmail,
        'X-Auth-Key' => $this->authKey,
      ],
    ];
    $this->purgeClient = static::httpClient($client_config);
  }

  /**
   * {@inheritdoc}
   */
  public function purgeUrls(array $urls) {
    $info = ['processed' => [], 'errors' => []];
    if (!$this->settingsOk) {
      $info['errors'] = $urls;
      return $info;
    }

    $this->initializePurgeClient();
    foreach (array_chunk($urls, 100, FALSE) as $urls_chunk) {
      $response = NULL;
      try {
        $response = $this->purgeClient->post($this->apiUrl, [
          RequestOptions::JSON => ['files' => $urls_chunk],
        ]);
      }
      catch (GuzzleException $e) {
        static::logger()->error(sprintf("Failed to purge urls in the Cloudflare cache. The error was: %s", $e->getMessage()));
      }
      if ($response && $response->getStatusCode() >= 200 && $response->getStatusCode() < 300) {
        $info['processed'] = array_merge($info['processed'], $urls_chunk);
      }
      else {
        $info['errors'] = array_merge($info['errors'], $urls_chunk);
      }
    }
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function purgeEverything() {
    if (!$this->settingsOk) {
      return FALSE;
    }

    $this->initializePurgeClient();
    $response = NULL;
    try {
      $response = $this->purgeClient->post($this->apiUrl, [
        RequestOptions::JSON => ['purge_everything' => TRUE],
      ]);
    }
    catch (GuzzleException $e) {
      static::logger()->error(sprintf("Failed to purge everything in the Cloudflare cache. The error was: %s", $e->getMessage()));
    }
    return $response && $response->getStatusCode() >= 200 && $response->getStatusCode() < 300;
  }

}
