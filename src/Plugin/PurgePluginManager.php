<?php

namespace Drupal\custom_purge\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * The custom purge plugin manager.
 */
class PurgePluginManager extends DefaultPluginManager {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A cached list of instantiated purge plugins.
   *
   * @var \Drupal\custom_purge\Plugin\PurgePluginInterface[]
   */
  protected $purgePlugins;

  /**
   * The PurgePluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/custom_purge/Purge', $namespaces, $module_handler, 'Drupal\custom_purge\Plugin\PurgePluginInterface', 'Drupal\custom_purge\Annotation\PurgePlugin');
    $this->alterInfo('custom_purge_plugin');
    $this->setCacheBackend($cache_backend, 'custom_purge_plugin');
    $this->purgePlugins = [];
  }

  /**
   * Get plugins of assigned cache instances for a given domain.
   *
   * @param string $domain
   *   The domain to get assigned purge plugins for.
   * @param array $caches
   *   (Optional) Use this argument to restrict to a certain subset of cache
   *   instances to purge. Example: ['type' => ['varnish']] Purges the urls only
   *   at known Varnish instances. ['name' => ['my_varnish_instance']] Purges
   *   the urls only at a known instance having the specified name.
   *
   * @return \Drupal\custom_purge\Plugin\PurgePluginInterface[]
   *   A list of purge plugins for the domain.
   */
  public function getPurgePluginsForDomain($domain, array $caches = []) {
    $config = $this->configFactory->get('custom_purge.settings');
    $profile_key = 'profiles.' . $config->get('profile') . '.';
    $assigned_cache_instances = NULL;
    $default_protocol = NULL;
    foreach ($config->get($profile_key . 'domains') as $domain_settings) {
      if ($domain_settings['domain'] === $domain) {
        $assigned_cache_instances = $domain_settings['assigned_cache_instances'];
        $default_protocol = $domain_settings['default_protocol'];
        break;
      }
    }
    if (is_null($assigned_cache_instances)) {
      foreach ($config->get($profile_key . 'domains') as $domain_settings) {
        if (!empty($domain_settings['is_default'])) {
          $assigned_cache_instances = $domain_settings['assigned_cache_instances'];
          $default_protocol = $domain_settings['default_protocol'];
          break;
        }
      }
    }
    if (empty($assigned_cache_instances)) {
      return [];
    }

    $plugins = [];
    foreach ($assigned_cache_instances as $cache_name) {
      foreach ($config->get($profile_key . 'cache_instances') as $cache_plugin_settings) {
        if ($cache_name !== $cache_plugin_settings['cache_name']) {
          continue;
        }

        $is_included = TRUE;
        if (!empty($caches['type']) || !empty($caches['name'])) {
          $is_included = FALSE;
          if (!empty($caches['type'])) {
            if (is_string($caches['type']) && $caches['type'] === $cache_plugin_settings['cache_type']) {
              $is_included = TRUE;
            }
            elseif (is_array($caches['type']) && in_array($cache_plugin_settings['cache_type'], $caches['type'])) {
              $is_included = TRUE;
            }
          }
          if (!$is_included && !empty($caches['name'])) {
            if (is_string($caches['name']) && $caches['name'] === $cache_plugin_settings['cache_name']) {
              $is_included = TRUE;
            }
            elseif (is_array($caches['name']) && in_array($cache_plugin_settings['cache_name'], $caches['name'])) {
              $is_included = TRUE;
            }
          }
        }
        if (!$is_included) {
          break;
        }

        $cache_name_domain_sensitive = $cache_name . '__' . $default_protocol . '__' . $domain;
        if (isset($this->purgePlugins[$cache_name])) {
          $plugins[] = $this->purgePlugins[$cache_name];
          break;
        }
        elseif (isset($this->purgePlugins[$cache_name_domain_sensitive])) {
          $plugins[] = $this->purgePlugins[$cache_name_domain_sensitive];
          break;
        }
        $plugin = $this->createInstance($cache_plugin_settings['cache_type'], $cache_plugin_settings);
        if ($plugin instanceof DomainSensitiveCacheInterface) {
          $plugin->setDomainSettings(new DomainSettings($domain_settings));
          $this->purgePlugins[$cache_name_domain_sensitive] = $plugin;
        }
        else {
          $this->purgePlugins[$cache_name] = $plugin;
        }
        $plugins[] = $plugin;
      }
    }

    return $plugins;
  }

  /**
   * Set the config factory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function setConfigFactory(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

}
