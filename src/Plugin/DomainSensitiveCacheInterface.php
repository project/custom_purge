<?php

namespace Drupal\custom_purge\Plugin;

/**
 * Interface for purge plugins for domain sensitive caching instances.
 *
 * Domain sensitive means, whether the cache instance can distinguish domains
 * from each other. Such an instance is capable of holding objects for
 * multiple domains, and calling it to purge everything would be in
 * scope of purging everything regards one certain domain.
 *
 * In order to purge for every known domain, a purge call would be executed
 * separately for each assigned domain. In contrary, on a cache instance
 * that is not domain sensitive, only one call in total would be executed,
 * regardless how many domains are assigned to that instance.
 */
interface DomainSensitiveCacheInterface {

  /**
   * Set the domain settings.
   *
   * @param \Drupal\custom_purge\Plugin\DomainSettings $settings
   *   The domain settings.
   */
  public function setDomainSettings(DomainSettings $settings);

  /**
   * Get the domain settings.
   *
   * @return \Drupal\custom_purge\Plugin\DomainSettings
   *   The domain settings.
   */
  public function getDomainSettings();

}
