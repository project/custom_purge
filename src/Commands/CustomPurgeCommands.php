<?php

namespace Drupal\custom_purge\Commands;

use Drupal\custom_purge\Purger;
use Drush\Commands\DrushCommands;

/**
 * Custom Purge Drush commands.
 */
class CustomPurgeCommands extends DrushCommands {

  /**
   * The Purger service.
   *
   * @var \Drupal\custom_purge\Purger
   */
  protected $purger;

  /**
   * Construct of CustomPurgeCommands.
   *
   * @param \Drupal\custom_purge\Purger $purger
   *   The Purger service.
   */
  public function __construct(Purger $purger) {
    parent::__construct();
    $this->purger = $purger;
  }

  /**
   * Purges a given URL.
   *
   * @param string $url
   *   The url to purge. Multiple urls can be given, separated by comma (,).
   * @param array $options
   *   An associative array of options.
   *
   * @option cache-type
   *   Only purge on a certain cache type, e.g. "varnish" or "cloudflare".
   *   Multiple values can be separated by comma (,).
   * @option cache-name
   *   Only purge on a certain cache instance name, e.g. "my_varnish_instance".
   *   Multiple values can be separated by comma (,).
   * @option enqueue
   *   Set this option if you want to use the configured delays in seconds like
   *   when purging everything. Items will be thrown into a queue (namely
   *   "custom_purge_urls") and then be executed on next cron or explicit
   *   queue:run command.
   * @option add-delay
   *   Additional delay in seconds, that will be added to every cache purge
   *   plugin (including those that are set to 0). This parameter will
   *   only take effect, when the enqueue option is set.
   *
   * @usage custom-purge:url https://www.example.com/my-content --enqueue
   *   Enqueues the purge of the url https://www.example.com/my-content on all
   *   caching instances that that are known from the current configuration.
   *
   * @command custom-purge:url
   */
  public function url(string $url, array $options = [
    'cache-type' => NULL,
    'cache-name' => NULL,
    'enqueue' => NULL,
    'add-delay' => NULL,
  ]) {
    $caches = [];
    if (!empty($options['cache-name'])) {
      foreach (explode(',', $options['cache-name']) as $cache_name) {
        $caches['name'][] = trim($cache_name);
      }
    }
    if (!empty($options['cache-type'])) {
      foreach (explode(',', $options['cache-type']) as $cache_type) {
        $caches['type'][] = trim($cache_type);
      }
    }
    $urls = [];
    foreach (explode(',', $url) as $to_purge) {
      $urls[] = trim($to_purge);
    }
    $enqueue = isset($options['enqueue']);
    $add_delay = isset($options['add-delay']) ? (int) $options['add-delay'] : 0;
    $result = $this->purger->purgeUrls($urls, $caches, $enqueue, $add_delay);
    if (!empty($result['processed'])) {
      foreach ($result['processed'] as $item_key => $processed_urls) {
        $this->io()->success(sprintf("Successfully purged %d url(s) from %s.", count($processed_urls), $item_key));
      }
    }
    if (!empty($result['skipped'])) {
      foreach ($result['skipped'] as $item_key => $skipped_urls) {
        $this->io()->writeln(sprintf("Skipped purging of %d url(s) from %s, as it is not allowed to purge single urls from it.", count($skipped_urls), $item_key));
      }
    }
    if (!empty($result['errors'])) {
      foreach ($result['errors'] as $item_key => $errored_urls) {
        $this->io()->error(sprintf("Failed to purge %d url(s) from %s. Please see the log for details.", count($errored_urls), $item_key));
      }
    }
    if (!empty($result['queued'])) {
      foreach ($result['queued'] as $item_key => $queued_urls) {
        $this->io()->success(sprintf("Successfully enqueued %d urls to be purged from %s.", count($queued_urls), $item_key));
      }
    }
  }

  /**
   * Enqueues purging of everything.
   *
   * This command reads from the "delay_complete_purge" for each configured
   * cache purge plugin. If that key is set to 0, then the complete purge is
   * being executed immediately. Any other plugin will be thrown into a queue
   * (namely "custom_purge_everything"), and is supposed to be executed on the
   * next cron or an explicit queue:run command.
   *
   * @param array $options
   *   An associative array of options.
   *
   * @option domain
   *   Only run for a specific domain (that is known from configuration).
   *   Multiple values can be separated by comma (,).
   * @option cache-type
   *   Only purge on a certain cache type, e.g. "varnish" or "cloudflare".
   *   Multiple values can be separated by comma (,).
   * @option cache-name
   *   Only purge on a certain cache instance name, e.g. "my_varnish_instance".
   *   Multiple values can be separated by comma (,).
   * @option add-delay
   *   Additional delay in seconds, that will be added to every cache purge
   *   plugin (including those that are set to 0). This might be handy if you
   *   want to just setup a complete purge operation that would be automatically
   *   executed at the specified delayed time.
   * @option ignore-existing-items
   *   Normally, this command checks for existing items in the queue and only
   *   adds cache purge plugins that are not yet added. When this option is set,
   *   then all items will be enqueued, regardless whether they are already in
   *   the queue.
   * @option reset-queue
   *   When this option is set, all remaining items (if any) will be dropped
   *   from the queue, before throwing cache purge plugins into it.
   * @option ignore-delay-run-immediately
   *   When this option is set, no items will be enqueued. Instead, all
   *   configured cache purge plugins (or the ones set by cache-type or
   *   cache-name option) will be executed immediately (in the sequential order
   *   of the configuration).
   *
   * @usage custom-purge:enqueue-purge-everything --domain=example.com --delay=3600 --ignore-existing-items
   *   This command would enqueue any operation to purge the whole cache
   *   of example.com (if properly configured), with an additional delay of one
   *   hour for every cache purge plugin and ignores already queued items.
   *
   * @command custom-purge:enqueue-purge-everything
   */
  public function enqueuePurgeEverything(array $options = [
    'domain' => NULL,
    'cache-type' => NULL,
    'cache-name' => NULL,
    'add-delay' => NULL,
    'ignore-existing-items' => NULL,
    'reset-queue' => NULL,
    'ignore-delay-run-immediately' => NULL,
  ]) {
    $domains = isset($options['domain']) ? explode(',', $options['domain']) : [];
    foreach ($domains as $i => $domain) {
      $domains[$i] = trim($domain);
    }
    $caches = [];
    if (!empty($options['cache-name'])) {
      foreach (explode(',', $options['cache-name']) as $cache_name) {
        $caches['name'][] = trim($cache_name);
      }
    }
    if (!empty($options['cache-type'])) {
      foreach (explode(',', $options['cache-type']) as $cache_type) {
        $caches['type'][] = trim($cache_type);
      }
    }
    $add_delay = isset($options['add-delay']) ? (int) $options['add-delay'] : 0;
    $ignore_existing_items = isset($options['ignore-existing-items']) && strtolower(trim((string) $options['ignore-existing-items'])) != 'no';
    $reset_queue = isset($options['reset-queue']) && strtolower(trim((string) $options['reset-queue'])) != 'no';
    $ignore_delay_run_immediately = isset($options['ignore-delay-run-immediately']) && strtolower(trim((string) $options['ignore-delay-run-immediately'])) != 'no';

    $result = $this->purger->enqueuePurgeEverything($domains, $caches, $add_delay, $ignore_existing_items, $reset_queue, $ignore_delay_run_immediately);
    if (!empty($result['processed'])) {
      $this->io()->success(sprintf("Successfully purged everything from %s.", implode(', ', array_keys($result['processed']))));
    }
    if (!empty($result['skipped'])) {
      $this->io()->writeln(sprintf("Skipped to purge everything from %s, as it is not allowed to purge everything from there.", implode(', ', array_keys($result['skipped']))));
    }
    if (!empty($result['errors'])) {
      $this->io()->error(sprintf("Failed to purge everything from %s.", implode(', ', array_keys($result['errors']))));
    }
    if (!empty($result['queued'])) {
      $this->io()->success(sprintf("Successfully enqueued to purge everything from %s.", implode(', ', array_keys($result['queued']))));
    }
  }

}
