# Custom Purge

Adds a form to purge Urls from configured caching instances.
Also provides Drush commands to enqueue purges of the whole cache.

## 0. Contents of this README

1. About this module
2. Requirements
3. Usage

## 1. About this module

This is a major refactor (might be called a fork) of
https://github.com/BurdaMagazinOrg/module-custom_purge. The config schema of
this refactor is not backwards-compatible, i.e. if you want to migrate to this
refactor, you'd need to adjust the configuration accordingly. Also, Drush
commands have changed due to the introduction of a new queue-based approach.

This refactor introduces the concept of queued purge operations, mainly when
purging everything from your beloved caching instances. When a purge operation
is to be run is mainly set via the delay_complete_purge configuration key (more
details see example below).

The module is built with a plugin system, so additional caching instances that
need to be purged can be added that way. Included purge plugins:

- Drupal's internal page cache
- Varnish (might be too custom / specific, you might need to write your own
  Varnish plugin)
- Cloudflare

## 2. Requirements

- Drush 9 or 10
- A working and regular Cron run or...
- ...alternatively, schedule a cron job that requlary runs
  `drush queue:run custom_purge_urls --items-limit=100` and
  `drush queue:run custom_purge_everything --items-limit=100`. One queue item is
  supposed to take approximately one second of processing time.

If you are using the `queue:run` command and ask yourself why so many items
were processed: In case an item did not reach the configured
delay_complete_purge yet, it re-throws itself into the queue (with additional
wait time of one second). It does only perform a purge request when that delay
is reached.

## 3. Usage

Since this module does not come with a form for the custom_purge.settings
configuration, you'd need to manually edit that yml file.
Example configuration of custom_purge.settings.yml:

```yaml
profile: production
profiles:
  production:
    max_url_per_request: 25
    flood_interval: 24
    flood_limit: 100
    cache_instances:
      -
        cache_type: drupal_page_cache
        cache_name: internal_page_cache
        allow_url_purge: true
        allow_purge_everything: true
        delay_complete_purge: 0
        cid_extensions:
          - ':'
          - ':html'
          - ':json'
          - ':xml'
          - ':xhtml'
      -
        cache_type: varnish
        cache_name: my_varnish
        allow_url_purge: true
        allow_purge_everything: true
        delay_complete_purge: 1800
        ip: 127.0.0.1
        port: 443
        verifyhost: false
        verifypeer: false
        single:
          http_method: DELETE
          http_headers: {  }
        everything:
          http_method: BAN
          http_headers: {  }
          url: '/ban-url'
      -
        cache_type: cloudflare
        cache_name: my_cloudflare
        allow_url_purge: true
        allow_purge_everything: true
        delay_complete_purge: 3600
        use_cf_settings: false
        email: cloudflaremail@localhost
        zone_id: '123456'
        apikey: fdghg5435sdfg
    domains:
      -
        is_default: true
        default_protocol: https
        domain: mydrupalsite.local
        assigned_cache_instances:
          - internal_page_cache
          - my_varnish
          - my_cloudflare
      -
        is_default: false
        default_protocol: https
        domain: anotherdomainofmydrupalsite.local
        assigned_cache_instances:
          - internal_page_cache
          - my_varnish_cache
          - my_varnish
```

When you're done, import that config with `drush cim`.
